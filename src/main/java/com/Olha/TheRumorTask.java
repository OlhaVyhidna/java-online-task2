package com.Olha;

/**
 * TheRumorTask class is used to find the total number of guests who have heard the rumor and the number of times that all
 * at the party (except Alice) heard the rumor before it stopped spreading
 *
 * @author Olha Vyhidna
 */

public class TheRumorTask {

    private int countOfFullSpreads;
    private int countOfGuestWhoHeard;

    private int numberOfGuests;
    private int numberOfChecks;


    public TheRumorTask(int numberOfGuests, int numberOfChecks) {
        this.numberOfGuests = numberOfGuests;
        this.numberOfChecks = numberOfChecks;

    }

    /**
     * This method count number guests who have heard the rumor and the number of times that all
     *  * at the party heard the rumor before it stopped spreading and initialize appropriate variables
     */

    public void startTheRumorTask() {

        for (int i = 0; i < getNumberOfChecks(); i++) {
            boolean guests[] = new boolean[getNumberOfGuests()];

            boolean theGuestAlreadyHeardTheRumor = false;

            int indexOfTheCurrentGuest = 0;
            int indexOfTheGuestWhoToldToCurrentOne = 0;
            guests[0] = true;     // the firs guest is the Bob, and he already heard the rumor


            while (!theGuestAlreadyHeardTheRumor) {
                int indexOfGuest = 0;
                do {
                    indexOfGuest = generateIndexOfGuest();
                } while ((indexOfGuest == indexOfTheCurrentGuest) || (indexOfGuest == indexOfTheGuestWhoToldToCurrentOne));

                indexOfTheGuestWhoToldToCurrentOne = indexOfTheCurrentGuest;
                indexOfTheCurrentGuest = indexOfGuest;

                if (guests[indexOfTheCurrentGuest]) {
                    theGuestAlreadyHeardTheRumor = true;
                } else {
                    guests[indexOfTheCurrentGuest] = true;
                    this.countOfGuestWhoHeard++;
                }
            }

            if (findIfGuestsAlreadyHeardThrRumor(guests)) {
                this.countOfFullSpreads++;
            }

        }

    }


    /**
     * This method generate random number in diapason of guests array size.
     *
     * @return index of the next guest.
     */


    private int generateIndexOfGuest() {
        return 1 + (int) (Math.random() * (numberOfGuests - 1));
    }


    /**
     *This method checks that all guests have heard the rumor.
     *
     * @param array of guests.
     * @return boolean result  of execution.
     *
     *
     */

    private boolean findIfGuestsAlreadyHeardThrRumor(boolean[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] != true) {
                return false;
            }
        }

        return true;
    }


    public int getNumberOfGuests() {
        return numberOfGuests;
    }

    public void setNumberOfGuests(int numberOfGuests) {
        this.numberOfGuests = numberOfGuests;
    }

    public int getNumberOfChecks() {
        return numberOfChecks;
    }

    public void setNumberOfChecks(int numberOfChecks) {
        this.numberOfChecks = numberOfChecks;
    }

    public int getCountOfFullSpreads() {
        return countOfFullSpreads;
    }

    public void setCountOfFullSpreads(int countOfFullSpreads) {
        this.countOfFullSpreads = countOfFullSpreads;
    }

    public int getCountOfGuestWhoHeard() {
        return countOfGuestWhoHeard;
    }

    public void setCountOfGuestWhoHeard(int countOfGuestWhoHeard) {
        this.countOfGuestWhoHeard = countOfGuestWhoHeard;
    }
}
