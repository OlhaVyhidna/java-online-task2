package com.Olha;

/**
 * StartTheRumorTask countestimate the probability that
 * everyone at the party (except Alice) will hear the rumor before it stops
 * propagating & an estimate of the expected number of people to hear
 * the rumor. And prints the results in the console
 *
 * @author Olha Vyhidna
 */
public class StartTheRumorTask
{
    public static void main( String[] args )
    {
        TheRumorTask theRumorTask = new TheRumorTask(10, 1000);
        theRumorTask.startTheRumorTask();

        int countOfFullSpread = theRumorTask.getCountOfFullSpreads();
        int countOfGuestWhoHeard = theRumorTask.getCountOfGuestWhoHeard();
        int numberOfChecks = theRumorTask.getNumberOfChecks();

        System.out.println("the probability that\n" +
                "everyone at the party (except Alice) will hear the rumor before it stops\n" +
                "propagating is - " + (((double)countOfFullSpread/numberOfChecks)*100)  + "%" );
        System.out.println("he expected number of people to hear\n" +
                "the rumor is - " + ((double)countOfGuestWhoHeard/numberOfChecks));
    }
}
